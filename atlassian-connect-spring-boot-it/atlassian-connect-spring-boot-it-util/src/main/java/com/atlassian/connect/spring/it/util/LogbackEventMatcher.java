package com.atlassian.connect.spring.it.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class LogbackEventMatcher extends TypeSafeMatcher<ILoggingEvent> {

    private final Matcher<Level> levelMatcher;
    private final Matcher<String> messageMatcher;

    public LogbackEventMatcher(Matcher<Level> levelMatcher, Matcher<String> messageMatcher) {
        this.levelMatcher = levelMatcher;
        this.messageMatcher = messageMatcher;
    }

    public static LogbackEventMatcher logbackEvent(Matcher<Level> levelMatcher, Matcher<String> messageMatcher) {
        return new LogbackEventMatcher(levelMatcher, messageMatcher);
    }

    @Override
    protected boolean matchesSafely(ILoggingEvent event) {
        return levelMatcher.matches(event.getLevel()) && messageMatcher.matches(event.getFormattedMessage());
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("an ILoggingEvent with a level ").appendValue(levelMatcher)
                .appendText(" and a message ").appendValue(messageMatcher);
    }
}
