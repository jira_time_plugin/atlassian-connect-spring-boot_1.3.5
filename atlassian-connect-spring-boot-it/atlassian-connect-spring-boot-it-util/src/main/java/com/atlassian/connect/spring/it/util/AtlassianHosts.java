package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;

public final class AtlassianHosts {

    public static final String CLIENT_KEY = "some-host";

    public static final String SHARED_SECRET = "some-secret";

    public static final String BASE_URL = "http://example.com/product";

    private AtlassianHosts() {}

    public static AtlassianHost createAndSaveHost(AtlassianHostRepository hostRepository) {
        AtlassianHost host = new AtlassianHostBuilder().build();
        hostRepository.save(host);
        return host;
    }
}
