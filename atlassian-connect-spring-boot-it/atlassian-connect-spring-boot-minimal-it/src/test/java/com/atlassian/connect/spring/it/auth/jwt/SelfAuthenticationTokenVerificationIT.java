package com.atlassian.connect.spring.it.auth.jwt;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.atlassian.connect.spring.it.util.FixedJwtSigningClientHttpRequestInterceptor;
import com.atlassian.connect.spring.it.util.SimpleJwtSigningRestTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SelfAuthenticationTokenVerificationIT extends BaseApplicationIT {

    @Autowired
    private SelfAuthenticationTokenGenerator selfAuthenticationTokenGenerator;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldReturnPrincipalDetailsForValidSelfAuthenticationTokenHeader() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String subject = "charlie";
        final String selfAuthenticationToken = selfAuthenticationTokenGenerator
                .createSelfAuthenticationToken(new AtlassianHostUser(host, Optional.of(subject)));
        TestRestTemplate restTemplate =
                new SimpleJwtSigningRestTemplate(new FixedJwtSigningClientHttpRequestInterceptor(selfAuthenticationToken));

        ResponseEntity<AtlassianHostUser> response = restTemplate.getForEntity(getRequestUri(), AtlassianHostUser.class);

        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserKey(), is(Optional.of(subject)));
    }

    @Test
    public void shouldReturnPrincipalDetailsForValidSelfAuthenticationTokenQueryParameter() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String subject = "charlie";
        final String selfAuthenticationToken = selfAuthenticationTokenGenerator
                .createSelfAuthenticationToken(new AtlassianHostUser(host, Optional.of(subject)));

        UriComponentsBuilder requestUriBuilder = getRequestUriBuilder();
        URI requestUri = requestUriBuilder.queryParam("jwt", selfAuthenticationToken).build().toUri();

        ResponseEntity<AtlassianHostUser> response = restTemplate.getForEntity(requestUri, AtlassianHostUser.class);
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
        assertThat(response.getBody().getHost().getClientKey(), is(host.getClientKey()));
        assertThat(response.getBody().getUserKey(), is(Optional.of(subject)));
    }

    private URI getRequestUri() {
        return getRequestUriBuilder().build().toUri();
    }

    private UriComponentsBuilder getRequestUriBuilder() {
        return UriComponentsBuilder.fromUri(URI.create(getServerAddress() + "/jwt"));
    }

    @TestConfiguration
    public static class SelfAuthenticationTokenVerificationConfiguration {

        @Bean
        public JwtVerificationIT.JwtPrincipalController jwtPrincipalController() {
            return new JwtVerificationIT.JwtPrincipalController();
        }
    }
}
