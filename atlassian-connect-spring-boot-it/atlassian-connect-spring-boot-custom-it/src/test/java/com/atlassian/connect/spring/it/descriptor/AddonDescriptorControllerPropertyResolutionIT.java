package com.atlassian.connect.spring.it.descriptor;

import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddonDescriptorControllerPropertyResolutionIT extends BaseApplicationIT {

    @Value("${add-on.key}")
    private String addonKey;

    @Test
    public void shouldReplacePropertyPlaceholdersInDescriptor() throws Exception {
        mvc.perform(get("/atlassian-connect.json")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.key").value(is(addonKey)));
    }
}
