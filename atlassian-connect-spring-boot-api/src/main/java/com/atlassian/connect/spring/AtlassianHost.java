package com.atlassian.connect.spring;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.Calendar;
import java.util.Objects;

/**
 * An Atlassian host in which the add-on is or has been installed. Hosts are stored in {@link AtlassianHostRepository}.
 *
 * <p>During processing of a request from an Atlassian host, the details of the host and of the user at the browser can
 * be obtained from the {@link AtlassianHostUser}.
 *
 * @since 1.0.0
 * @see AtlassianHostRepository
 */
public class AtlassianHost {

    @Id
    private String clientKey;                   // unique-client-identifier"

    private String publicKey;                   // "MIGf....ZRWzwIDAQAB"

    private String oauthClientId;               // "OpaQUeCliEnt1DStR1Ng"

    private String sharedSecret;                // "a-secret-key-not-to-be-lost"

    private String baseUrl;                     // "http://example.atlassian.net"

    private String productType;                 // "jira"

    private String description;                 // "Atlassian JIRA at https://example.atlassian.net"

    private String serviceEntitlementNumber;    // "SEN-number"

    private boolean addonInstalled;

    @CreatedDate
    private Calendar createdDate;

    @LastModifiedDate
    private Calendar lastModifiedDate;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String lastModifiedBy;

    /**
     * Creates a new empty Atlassian host (used by persistence mechanism).
     */
    public AtlassianHost() {}

    /**
     * The identifying key for the Atlassian product instance that the add-on was installed into.
     * This will never change for a given instance, and is unique across all Atlassian product tenants.
     * This value should be used to key tenant details in your add-on.
     *
     * @return the client key of the host
     */
    public String getClientKey() {
        return clientKey;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setClientKey(String clientKey) {
        this.clientKey = clientKey;
    }

    /**
     * The public key of the Atlassian product instance.
     *
     * @deprecated No replacement.
     * @return the public key of the host
     */
    public String getPublicKey() {
        return publicKey;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    /**
     * Returns the client identifier for use with OAuth 2.0 authentication.
     *
     * @return the OAuth 2.0 client ID
     */
    public String getOauthClientId() {
        return oauthClientId;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setOauthClientId(String oauthClientId) {
        this.oauthClientId = oauthClientId;
    }

    /**
     * The shared secret issued by the Atlassian product instance on installation of the add-on. This string is used
     * to validate incoming JWT tokens and to sign outgoing JWT tokens.
     *
     * @return the shared secret of the installation
     */
    public String getSharedSecret() {
        return sharedSecret;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    /**
     * The URL prefix for the Atlassian product instance. All of its REST endpoints begin with this URL.
     *
     * @return the base URL of the host
     */
    public String getBaseUrl() {
        return baseUrl;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * The identifier of the category of Atlassian product.
     *
     * @return the host product type, e.g. <code>jira</code> or <code>confluence</code>
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /**
     * The host product description. This string is customisable by an instance administrator.
     *
     * @return the description of the host
     */
    public String getDescription() {
        return description;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * The Support Entitlement Number (SEN) is the add-on license identifier.
     * This attribute will only be included during installation of a paid add-on.
     *
     * @return the SEN
     */
    public String getServiceEntitlementNumber() {
        return serviceEntitlementNumber;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setServiceEntitlementNumber(String serviceEntitlementNumber) {
        this.serviceEntitlementNumber = serviceEntitlementNumber;
    }

    /**
     * Returns true if the add-on is currently installed on the host. Upon uninstallation, the value of this flag will
     * be set to <code>false</code>.
     *
     * @return true if the add-on is installed
     */
    public boolean isAddonInstalled() {
        return addonInstalled;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setAddonInstalled(boolean addonInstalled) {
        this.addonInstalled = addonInstalled;
    }

    /**
     * The time at which the add-on was first installed on the host.
     *
     * @return the creation time
     */
    public Calendar getCreatedDate() {
        return createdDate;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setCreatedDate(Calendar createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * The time at which the add-on installation was last modified.
     *
     * @return the last modification time
     */
    public Calendar getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setLastModifiedDate(Calendar lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    /**
     * The key of the Atlassian user that first installed the add-on on the host, if any.
     *
     * @return the user key of the creator
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * The key of the Atlassian user that last modified the add-on installation, if any.
     *
     * @return the user key of the last modifier
     */
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    /**
     * Used by persistence mechanism.
     */
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AtlassianHost that = (AtlassianHost) o;
        return addonInstalled == that.addonInstalled &&
                Objects.equals(clientKey, that.clientKey) &&
                Objects.equals(publicKey, that.publicKey) &&
                Objects.equals(oauthClientId, that.oauthClientId) &&
                Objects.equals(sharedSecret, that.sharedSecret) &&
                Objects.equals(baseUrl, that.baseUrl) &&
                Objects.equals(productType, that.productType) &&
                Objects.equals(description, that.description) &&
                Objects.equals(serviceEntitlementNumber, that.serviceEntitlementNumber);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(clientKey, publicKey, oauthClientId, sharedSecret, baseUrl, productType, description, serviceEntitlementNumber, addonInstalled);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format("AtlassianHost{clientKey='%s', baseUrl='%s', productType='%s', addonInstalled=%s}",
                clientKey, baseUrl, productType, addonInstalled);
    }
}
