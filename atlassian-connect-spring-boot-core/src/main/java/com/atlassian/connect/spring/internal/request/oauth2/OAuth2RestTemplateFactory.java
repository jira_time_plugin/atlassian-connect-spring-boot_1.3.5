package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class OAuth2RestTemplateFactory {

    private static final URI DEV_AUTHORIZATION_SERVER_URL = URI.create("https://auth.dev.atlassian.io");

    private static final URI PRODUCTION_AUTHORIZATION_SERVER_URL = URI.create("https://auth.atlassian.io");

    private final OAuth2JwtAssertionGenerator jwtAssertionGenerator;

    private final RestTemplateBuilder restTemplateBuilder;

    private final RestTemplate accessTokenProviderRestTemplate;

    private final String atlassianConnectClientVersion;

    @Autowired
    public OAuth2RestTemplateFactory(OAuth2JwtAssertionGenerator jwtAssertionGenerator,
            RestTemplateBuilder restTemplateBuilder,
            @Value("${atlassian.connect.client-version}") String atlassianConnectClientVersion) {
        this.jwtAssertionGenerator = jwtAssertionGenerator;
        this.restTemplateBuilder = restTemplateBuilder;
        this.accessTokenProviderRestTemplate = restTemplateBuilder.build();
        this.atlassianConnectClientVersion = atlassianConnectClientVersion;
    }

    @Cacheable(value = {"oauth-2-clients"})
    public OAuth2RestTemplate getOAuth2RestTemplate(AtlassianHostUser hostUser) {
        assertValidHostUser(hostUser);

        URI authorizationServerBaseUrl = getAuthorizationServerBaseUrl(hostUser.getHost());
        OAuth2ProtectedResourceDetails protectedResourceDetails = createProtectedResourceDetails(hostUser, authorizationServerBaseUrl);
        OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(protectedResourceDetails);
        restTemplateBuilder.configure(oAuth2RestTemplate);
        oAuth2RestTemplate.setAccessTokenProvider(createAccessTokenProvider(hostUser, authorizationServerBaseUrl));
        oAuth2RestTemplate.getInterceptors().add(createRequestInterceptor(hostUser));
        return oAuth2RestTemplate;
    }

    /**
     * Public to enable integration testing.
     *
     * @return the rest template used for access token requests
     */
    public RestTemplate getAccessTokenProviderRestTemplate() {
        return accessTokenProviderRestTemplate;
    }

    private void assertValidHostUser(AtlassianHostUser hostUser) {
        if (StringUtils.isBlank(hostUser.getHost().getOauthClientId())) {
            throw new UnsupportedOperationException("Can not act as a user for a host with no OAuthClientId. " +
                    "Make sure you have ACT_AS_USER scope specified in your descriptor.");
        }
        if (!hostUser.getUserKey().isPresent()) {
            throw new IllegalArgumentException("The provided AtlassianHostUser did not specify a user to act as.");
        }
    }

    private URI getAuthorizationServerBaseUrl(AtlassianHost host) {
        boolean isDevHost = URI.create(host.getBaseUrl()).getHost().endsWith(".jira-dev.com");
        return isDevHost ? DEV_AUTHORIZATION_SERVER_URL : PRODUCTION_AUTHORIZATION_SERVER_URL;
    }

    private OAuth2ProtectedResourceDetails createProtectedResourceDetails(AtlassianHostUser hostUser, URI authorizationServerBaseUrl) {
        return new JwtBearerResourceDetails(
                hostUser.getHost().getClientKey(),
                hostUser.getHost().getSharedSecret(),
                authorizationServerBaseUrl.toASCIIString());
    }

    private JwtBearerAccessTokenProvider createAccessTokenProvider(AtlassianHostUser hostUser, URI authorizationServerBaseUrl) {
        return new JwtBearerAccessTokenProvider(hostUser, authorizationServerBaseUrl,
                accessTokenProviderRestTemplate, jwtAssertionGenerator);
    }

    private OAuth2HttpRequestInterceptor createRequestInterceptor(AtlassianHostUser hostUser) {
        return new OAuth2HttpRequestInterceptor(hostUser.getHost(), atlassianConnectClientVersion);
    }
}
