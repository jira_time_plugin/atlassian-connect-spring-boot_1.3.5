package com.atlassian.connect.spring.internal.auth;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * A helper class for obtaining the current authenticated {@link AtlassianHostUser}.
 */
@Component
public class AtlassianConnectSecurityContextHelper {

    public Optional<AtlassianHost> getHostFromSecurityContext() {
        return getHostUserFromSecurityContext().map(AtlassianHostUser::getHost);
    }

    public Optional<AtlassianHostUser> getHostUserFromSecurityContext() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .map(Authentication::getPrincipal)
                .filter(AtlassianHostUser.class::isInstance)
                .map(AtlassianHostUser.class::cast);
    }
}
