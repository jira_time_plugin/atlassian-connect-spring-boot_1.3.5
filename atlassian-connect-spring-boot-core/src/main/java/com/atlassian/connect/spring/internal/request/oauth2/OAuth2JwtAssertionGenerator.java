package com.atlassian.connect.spring.internal.request.oauth2;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.jwt.JwtJsonBuilder;
import com.atlassian.connect.spring.internal.jwt.JwtWriter;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.crypto.MACSigner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Optional;

@Component
public class OAuth2JwtAssertionGenerator {

    private static final Logger log = LoggerFactory.getLogger(OAuth2JwtAssertionGenerator.class);

    public String getAssertionString(AtlassianHostUser hostUser, URI authorizationServerUrl) {
        long iat = System.currentTimeMillis() / 1000;
        JwtJsonBuilder jsonBuilder = new JwtJsonBuilder();
        Optional<String> maybeUserKey = hostUser.getUserKey();
        if (!maybeUserKey.isPresent()) {
            throw new IllegalArgumentException("The key of a user to act as must be provided");
        }
        String json = jsonBuilder
                .issuedAt(iat)
                .expirationTime(iat + 60L) // Assertions have a maximum life span of 60 seconds
                .issuer("urn:atlassian:connect:clientid:" + hostUser.getHost().getOauthClientId())
                .subject("urn:atlassian:connect:userkey:" + maybeUserKey.get())
                .audience(authorizationServerUrl.toASCIIString())
                .claim("tnt", hostUser.getHost().getBaseUrl())
                .build();
        log.debug("Created OAuth 2.0 JWT assertion: {}", json);
        return createJwtWriter(hostUser.getHost().getSharedSecret()).jsonToJwt(json);
    }

    private JwtWriter createJwtWriter(String secret) {
        return new JwtWriter(JWSAlgorithm.HS256, new MACSigner(secret));
    }
}
